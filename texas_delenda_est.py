import random

from playwright.sync_api import sync_playwright

law_violated = ["The story is told of an automaton constructed in such a way that it could play a winning game of chess, answering each move of an opponent with a countermove. A puppet in Turkish attire and with a hookah in its mouth sat before a chessboard placed on a large table. A system of mirrors created the illusion that this table was transparent from all sides. Actually, a little hunchback who was an expert chess player sat inside and guided the puppet’s hand by means of strings. One can imagine a philosophical counterpart to this device. The puppet called ‘historical materialism’ is to win all the time. It can easily be a match for anyone if it enlists the services of theology, which today, as we know, is wizened and has to keep out of sight.", "‘One of the most remarkable characteristics of human nature,’ writes Lotze, ‘is, alongside so much selfishness in specific instances, the freedom from envy which the present displays toward the future.’ Reflection shows us that our image of happiness is thoroughly colored by the time to which the course of our own existence has assigned us. The kind of happiness that could arouse envy in us exists only in the air we have breathed, among people we could have talked to, women who could have given themselves to us. In other words, our image of happiness is indissolubly bound up with the image of redemption. The same applies to our view of the past, which is the concern of history. The past carries with it a temporal index by which it is referred to redemption. There is a secret agreement between past generations and the present one. Our coming was expected on earth. Like every generation that preceded us, we have been endowed with a weak Messianic power, a power to which the past has a claim. That claim cannot be settled cheaply. Historical materialists are aware of that.", "A chronicler who recites events without distinguishing between major and minor ones acts in accordance with the following truth: nothing that has ever happened should be regarded as lost for history. To be sure, only a redeemed mankind receives the fullness of its past-which is to say, only for a redeemed mankind has its past become citable in all its moments. Each moment it has lived becomes a citation a l'ordre du jour — and that day is Judgment Day.", "The class struggle, which is always present to a historian influenced by Marx, is a fight for the crude and material things without which no refined and spiritual things could exist. Nevertheless, it is not in the form of the spoils which fall to the victor that the latter make their presence felt in the class struggle. They manifest themselves in this struggle as courage, humor, cunning, and fortitude. They have retroactive force and will constantly call in question every victory, past and present, of the rulers. As flowers turn toward the sun, by dint of a secret heliotropism the past strives to turn toward that sun which is rising in the sky of history. A historical materialist must be aware of this most inconspicuous of all transformations.", "The true picture of the past flits by. The past can be seized only as an image which flashes up at the instant when it can be recognized and is never seen again. ‘The truth will not run away from us’: in the historical outlook of historicism these words of Gottfried Keller mark the exact point where historical materialism cuts through historicism. For every image of the past that is not recognized by the present as one of its own concerns threatens to disappear irretrievably. (The good tidings which the historian of the past brings with throbbing heart may be lost in a void the very moment he opens his mouth.)", "To articulate the past historically does not mean to recognize it ‘the way it really was’ (Ranke). It means to seize hold of a memory as it flashes up at a moment of danger. Historical materialism wishes to retain that image of the past which unexpectedly appears to man singled out by history at a moment of danger. The danger affects both the content of the tradition and its receivers. The same threat hangs over both: that of becoming a tool of the ruling classes. In every era the attempt must be made anew to wrest tradition away from a conformism that is about to overpower it. The Messiah comes not only as the redeemer, he comes as the subduer of Antichrist. Only that historian will have the gift of fanning the spark of hope in the past who is firmly convinced that even the dead will not be safe from the enemy if he wins. And this enemy has not ceased to be victorious."]

evidence_obtained = ["From God Himself", "I percieved it through the universe", "The wind", "Rumours", "Small talk at the water cooler", "Why I found it inside myself", "On your phone"]

doctor = ["Hell", "Your mum", "Joe Biden"]

city = ["Austin", "Dallas", "Houston"]

state = ["TX"]

zip_code = str(random.choice(range(75000, 79000)))

county = ["Anderson", "Travis", "Dallas"]

playwright = sync_playwright().start()
browser = playwright.chromium.launch(headless=False)
page = browser.new_page()
page.goto("https://prolifewhistleblower.com/anonymous-form/")
page.fill('//*[@id="forminator-field-textarea-1"]', random.choice(law_violated))
page.fill('//*[@id="forminator-field-text-1"]', random.choice(evidence_obtained))
page.fill('//*[@id="forminator-field-text-6"]', random.choice(doctor))
page.fill('//*[@id="forminator-field-text-2"]', random.choice(city))
page.fill('//*[@id="forminator-field-text-3"]', random.choice(state))
page.fill('//*[@id="forminator-field-text-4"]', zip_code)
page.fill('//*[@id="forminator-field-text-5"]', random.choice(county))
page.click('label.forminator-checkbox:nth-child(3) > span:nth-child(2)')
page.click('.forminator-button')
